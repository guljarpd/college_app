import React from 'react';
import './App.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { DashboardRoute, CollegeRoute, StudentRoute } from './Components';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<DashboardRoute />} />
        <Route path="colleges" element={<CollegeRoute />} /> 
        <Route path="/students" element={<StudentRoute />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
