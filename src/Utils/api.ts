import axios from "axios";

export const getData = async(url: string, params: any) => {
    try {
        const data = await axios.get(url, params);
        console.log('getData =>', data);
        
        // if (data && data.data) {
        //     return data.data;
        // } else {
        //    return {

        //    } 
        // }
    } catch (error: any) {
        console.error('getData Error=>', error);
    }
}