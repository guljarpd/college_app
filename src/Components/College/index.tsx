import React, {useEffect, useState} from 'react';
import { useQuery } from '../../Utils/hooks';
import { CollegeI } from '../../Interfaces';
import axios from 'axios';
import CollegeTable from './Table';


export default function College() {

  const queryParams = useQuery();
  const cityName = queryParams.get('cityName');
  const [getColleges, setColleges] = useState<CollegeI[]>([]);
  const [getError, setError] = useState<string>('');
  const baseurl = process.env.REACT_APP_API_BASEURL;
  // 
  const getCollegeData = (city: string| null) => {
    // check from local state.
    let localStorageCollegeData = localStorage.getItem('CollegeList');
    if (city && localStorageCollegeData) {
      let data: CollegeI[] = JSON.parse(localStorageCollegeData)
      let filteredList = data.filter(item => {
        if (item.cityName.toLowerCase() === city.toLowerCase()) return item;
      })
      setColleges(filteredList);
      return;
    }else if(localStorageCollegeData){
      let data: CollegeI[] = JSON.parse(localStorageCollegeData)
      setColleges(data);
      return;
    }
    // 
    const url: string = '/api/v1/college/all';
    console.log('url=>', url);
    if(baseurl){
      const fullUrl: string = baseurl+url;
      axios.get(fullUrl).then(res => {
        // console.log('res', resData);
        const resData: CollegeI[] = res.data.data;
        if(resData) {
          if(city) {
            let filteredList = resData.filter(item => {
              if (item.cityName.toLowerCase() === city.toLowerCase()) return item;
            });
            setColleges(filteredList);
          } else {
            setColleges(resData);
          }
          // 
          localStorage.setItem('CollegeList', JSON.stringify(resData));
        }
      }).catch(err=>{
        console.error('err', err);
      });
    }
  }


  let tableData = getColleges.map(item => {
      delete item?.coursesOffered;
      return item;
  });


  useEffect(()=> {
    getCollegeData(cityName);
  }, [cityName])

  // default
  useEffect(()=> {
    getCollegeData(null);
  }, []);
   
  return (
    <React.Fragment>
      <CollegeTable data = {tableData}/>
    </React.Fragment>
  );
}
