import React, {useState, useEffect} from 'react';
import {
    Button,
    TextField,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Select,
    MenuItem,
    FormControl,
    InputLabel,
    Input,
    Chip,
    Snackbar
} from '@material-ui/core';

import { createStyles, makeStyles, useTheme, Theme } from '@material-ui/core/styles';
import axios from 'axios';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    chips: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    chip: {
      margin: 2,
    },
    noLabel: {
      marginTop: theme.spacing(3),
    },
  }),
);

// interfaces
interface AddCollegeProps {
    openModel: boolean;
    onModelClose: () => void;
}

interface Locations {
    countryName: string;
    cityName: string;
    stateName: string;
}

interface CollegeForm {
    name?: string;
    yearFounded?: string;
    coursesOffered?: string[];
    cityName?: string;
    stateName?: string;
    countryName?: string;
}

const locationsList: Locations[] = [
    {
        countryName: "India",
        cityName: "Mumbai",
        stateName: "Maharashtra",
    },
    {
        countryName: "India",
        cityName: "Delhi",
        stateName: "Delhi",
    },
    {
        countryName: "India",
        cityName: "Bengaluru",
        stateName: "Karnataka",
    },
    {
        countryName: "India",
        cityName: "Chennai",
        stateName: "Tamilnadu",
    },
    {
        countryName: "India",
        cityName: "Hyderabad",
        stateName: "Telangana",
    }
];

const coursesList: string[] = [
	"Architectural Engineering",
	"Civil Engineering",
	"Electrical Engineering",
	"Mechanical engineering",
	"Information Technology",
	"Computer Science Engineering",
	"Agriculture Engineering",
	"Aerospace Engineering",
	"Biomedical Engineering",
];

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const  getStyles = (name: string, personName: string[], theme: Theme) => {
    return {
      fontWeight:
        personName.indexOf(name) === -1
          ? theme.typography.fontWeightRegular
          : theme.typography.fontWeightMedium,
    };
}

export const Alert = (props: AlertProps) => {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}




const AddCollege = (props: AddCollegeProps) => {
  const [open, setOpen] = useState(true);
  const [formData, setFormData] = useState<CollegeForm>();
  const [getCourses, setCourses] = useState<string[]>([]);
  const [successMsg, setSuccessMsg] = useState<string>('');
  const [errorMsg, setErrorMsg] = useState<string>('');
  const [alertOpen, setAlertOpen] = useState<boolean>(false);
  //   
  const classes = useStyles();
  const theme = useTheme();
  
  //   
  useEffect(() => {
    setOpen(props.openModel);
  }, [props])

  // Functions
  const handleSave = () => {
    const baseurl = process.env.REACT_APP_API_BASEURL;
    const fullUrl = baseurl+'/api/v1/college/add';
    // college to db.
    // console.log('handleSave', formData);
    axios.post(fullUrl, formData)
        .then((res) => {
            if(res.data.data) {
                // show success message.
                setSuccessMsg('Success! College Added.')
                setAlertOpen(true);
            }
        })
        .catch((err) => {
            setErrorMsg(err?.response.data.error.message)
            setAlertOpen(true);
        })
  };

  const handleClose = () => {
    setOpen(false);
    props.onModelClose();
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    console.log(event.target?.name);
    setFormData(
        {
            ...formData,
            [event.target?.name]: event.target?.value
        }
    )
  };

  const handleSelectChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    console.log(event.target);
    let data = locationsList.filter(item => {
        if (item.cityName === event.target.value) return item;
    });
    if(data.length === 1) {
        setFormData({
            ...formData,
            cityName: data[0].cityName,
            stateName: data[0].stateName,
            countryName: data[0].countryName
        })
    }
  }

  const handleChipChange = async (event: React.ChangeEvent<{ value: unknown }>) => {
    setCourses(event.target.value as string[]);
    // 
    setFormData({
        ...formData,
        coursesOffered: event.target.value as string[]
    });
  };

  const handleCloseAlert = () => {
    setAlertOpen(false);
    setSuccessMsg('');
    setErrorMsg('');
  }

  return (
    <React.Fragment>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Add College</DialogTitle>
        {errorMsg !== ''? (
          <Snackbar open={alertOpen} autoHideDuration={6000} onClose={handleCloseAlert}>
            <Alert onClose={handleCloseAlert} severity="error">
              {errorMsg}
            </Alert>
          </Snackbar>
        ) : null}
        {successMsg !== ''? (
          <Snackbar open={alertOpen} autoHideDuration={6000} onClose={handleCloseAlert}>
            <Alert onClose={handleCloseAlert} severity="success">
              {successMsg}
            </Alert>
          </Snackbar>
        ) : null}
        <DialogContent>
          <DialogContentText>
            Add a new college to database.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="College Name"
            type="text"
            name="name"
            value={formData?.name}
            fullWidth
            onChange={handleInputChange}
          />
          <FormControl 
            margin="dense"
            fullWidth
          >
            <InputLabel id="demo-simple-select-label">City Name</InputLabel>
            <Select
                labelId="demo-simple-select-helper-label"
                id="demo-simple-select-helper"
                value={formData?.cityName}
                name="cityName"
                onChange={handleSelectChange}
            >
                {locationsList.map((item)=>{
                    return(<MenuItem value={item.cityName}>{item.cityName}</MenuItem>)
                })}
            </Select>
          </FormControl>
          <TextField
            margin="dense"
            id="yearFounded"
            name="yearFounded"
            label="Year Founded"
            type="number"
            value={formData?.yearFounded}
            fullWidth
            onChange={handleInputChange}
          />
           <FormControl margin="dense" fullWidth>
                <InputLabel id="demo-mutiple-chip-label">Courses</InputLabel>
                <Select
                labelId="demo-mutiple-chip-select-label"
                id="demo-mutiple-chip"
                multiple
                value={getCourses}
                onChange={handleChipChange}
                input={<Input id="select-multiple-chip" />}
                renderValue={(selected) => (
                    <div className={classes.chips}>
                    {(selected as string[]).map((value) => (
                        <Chip key={value} label={value} className={classes.chip} />
                    ))}
                    </div>
                )}
                MenuProps={MenuProps}
                >
                {coursesList.map((name) => (
                    <MenuItem key={name} value={name} style={getStyles(name, getCourses, theme)}>
                    {name}
                    </MenuItem>
                ))}
                </Select>
            </FormControl>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
          <Button onClick={handleSave} color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}

export default AddCollege;
