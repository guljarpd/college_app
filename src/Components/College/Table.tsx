import React, {useEffect, useState} from 'react';
import { 
    Paper,
    Toolbar, 
    Typography, 
    Tooltip,
    IconButton,
    TablePagination,
    TableSortLabel,
    TableContainer,
    Table,
    TableBody,
    TableRow,
    TableCell,
    TableHead
} from '@material-ui/core';
import {
    AddCircleSharp
} from '@material-ui/icons';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AddCollege from './AddCollege';
import ShowCollege from './ShowDetails';
// Interfaces
interface TableDataCollege {
    _id: string;
    name: string;
    yearFounded: string;
    cityName: string;
    stateName: string;
    countryName: string;
    studentCount: number;
}
interface TableDataProps {
    data: TableDataCollege[];
}

type Order = 'asc' | 'desc';

interface TableToolbarProps {
    tableTitle: string;
}

interface TableHeadProps {
    classes: ReturnType<typeof useToolbarStyles>;
    onRequestSort: (event: React.MouseEvent<unknown>, property: keyof TableDataCollege) => void;
    order: Order;
    orderBy: string;
}

interface HeadCell {
    id: keyof TableDataCollege,
    label: string;
}

// Custom css
const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
        width: '100%',
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 750,
    },
    title: {
        flex: '1 1 100%',
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
    header2: {
        fontWeight: 600
    }
  }),
);

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
}

function getComparator<Key extends keyof any>(
    order: Order,
    orderBy: Key,
  ): (a: { [key in Key]: number | string }, b: { [key in Key]: number | string }) => number {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
}
  
function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
    const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}



const TableToolbar = (props: TableToolbarProps) => {
    const classes = useToolbarStyles();
    const [getModel, setModel] = useState<boolean>(false);
    // 
    const handleClick = () => {
        setModel(true);
    }
    const closeModel = () => {
        setModel(false);
    }
    return (
      <React.Fragment>
        {getModel? <AddCollege openModel={true} onModelClose={closeModel} />: null}
        <Toolbar>
            <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
                {props.tableTitle}
            </Typography>
            <Tooltip title="Add College">
                <IconButton 
                    color={'secondary'} 
                    aria-label="Add College"
                    onClick={handleClick}
                >
                    <AddCircleSharp />
                </IconButton>
            </Tooltip>
        </Toolbar>
      </React.Fragment>
    );
};

const headCells: HeadCell[] = [
    {id: 'name', label: 'Name'},
    {id: 'cityName', label: 'City Name'},
    {id: 'stateName', label: 'State Name'},
    {id: 'yearFounded', label: 'Founded Year'}
]
  
const CustomTableHead = (props: TableHeadProps) => {
    const { classes, order, orderBy, onRequestSort } = props;
    const createSortHandler = (property: keyof TableDataCollege) => (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };
    return (
        <TableHead>
          <TableRow>
            {headCells.map((item) => (
              <TableCell
                key={item.id}
                sortDirection={orderBy === item.id ? order : false}
              >
                <TableSortLabel
                  className={classes.header2}
                  active={orderBy === item.id}
                  direction={orderBy === item.id ? order : 'asc'}
                  onClick={createSortHandler(item.id)}
                >
                  {item.label}
                  {orderBy === item.id ? (
                    <span className={classes.visuallyHidden}>
                      {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                    </span>
                  ) : null}
                </TableSortLabel>
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
    );
}


const CollegeTable = (props: TableDataProps) => {
    // Stores
    const [collegeData, setCollegeData] = useState<TableDataCollege[]>([]);
    const [rowsPerPage, setRowsPerPage] = useState<number>(10);
    const [page, setPage] = useState<number>(0);
    const [order, setOrder] = useState<Order>('asc');
    const [orderBy, setOrderBy] = useState<keyof TableDataCollege>('name');
    const [selectedCollegeId, setSelectedCollegeId] = useState<string | null>(null);
  
    // Styles
    const classes = useToolbarStyles();
    // Functions
    const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof TableDataCollege) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
    };
    // 
    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleClick = (event: React.MouseEvent<unknown>, id: any) => {
        // when click on this/ Open an Modal pop.
        console.log('handleClick=>', id);
        setSelectedCollegeId(id);
    }
    const closeModel = () => {
        setSelectedCollegeId('');
    }
    // 
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, collegeData.length - page * rowsPerPage);
    // 
    let sortedCollegeData = stableSort(
        collegeData, 
        getComparator(order, orderBy)
    ).slice(
        (page * rowsPerPage), 
        (page * rowsPerPage + rowsPerPage)
    );
    // 
    useEffect(() => {
        setCollegeData(props.data);
    }, [props.data]);

    return (
        <div className={classes.root}>
            {selectedCollegeId? (
                <ShowCollege 
                    id={selectedCollegeId} 
                    openModel={true} 
                    onModelClose={closeModel} 
                />)
                : null
            }
            <Paper className={classes.paper}>
                <TableToolbar tableTitle={'Colleges'} />
                <TableContainer> 
                   <Table
                    className={classes.table}
                    aria-labelledby="tableTitle"
                    size={'medium'}
                    aria-label="enhanced table"
                   >   <CustomTableHead 
                            classes={classes}
                            order={order}
                            orderBy={orderBy}
                            onRequestSort={handleRequestSort}
                        />
                       <TableBody>
                            {sortedCollegeData.map((row, _index) => {
                                return (
                                    <TableRow
                                        hover={true}
                                        onClick={(event) => handleClick(event, row._id)}
                                        role="button"
                                        key={row._id}
                                    >
                                        <TableCell align="left">{row.name}</TableCell>
                                        <TableCell align="left">{row.cityName}</TableCell>
                                        <TableCell align="left">{row.stateName}</TableCell>
                                        <TableCell align="left">{row.yearFounded}</TableCell>
                                    </TableRow>
                                );
                            })}
                            {emptyRows > 0 && (
                                <TableRow style={{ height: (53 * emptyRows) }}>
                                  <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>
                    </Table> 
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 15]}
                    component="div"
                    count={collegeData.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </Paper>
        </div>
    )
}

export default CollegeTable;