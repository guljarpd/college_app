import React, {useState, useEffect} from 'react';
import {
    Button,
    Dialog,
    DialogActions,
    DialogTitle,
    Chip,
    Grid,
    Container,
    Typography,
    FormLabel,
    Divider,
    List,
    ListItem,
    ListItemText,
} from '@material-ui/core';

import { createStyles, makeStyles, useTheme, Theme } from '@material-ui/core/styles';
import axios from 'axios';
import { CollegeI } from '../../Interfaces';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    chips: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    chip: {
      margin: 2,
    },
    noLabel: {
      marginTop: theme.spacing(3),
    },
    divider: {
      marginTop: 10,
      marginBottom: 10
    }
  }),
);

// interfaces
interface ShowCollegeProps {
    openModel: boolean;
    id: string;
    onModelClose: () => void;
}

const  getStyles = (name: string, personName: string[], theme: Theme) => {
    return {
      fontWeight:
        personName.indexOf(name) === -1
          ? theme.typography.fontWeightRegular
          : theme.typography.fontWeightMedium,
    };
}


const ShowCollege = (props: ShowCollegeProps) => {
  const [open, setOpen] = useState(true);
  const [collegeData, setCollegeData] = useState<CollegeI | null>(null);
  const [similarColleges, setSimilarColleges] = useState<CollegeI[]>([]);
  const [errorMsg, setErrorMsg] = useState<string>('');
  //   
  const classes = useStyles();
  const theme = useTheme();

  const getCollegeDetails = (id: string) => {
    const baseurl = process.env.REACT_APP_API_BASEURL;
    const fullUrl = baseurl+'/api/v1/college/details/'+id;
    axios.get(fullUrl).then((res) => {
      let data: CollegeI = res.data.data;
      setCollegeData(data);
      let courseName = data.coursesOffered? data.coursesOffered[0] : "";
      getSimilarColleges(data.cityName, data.stateName,courseName, data.studentCount);
    }).catch(err => {
        setErrorMsg(err?.response.data.error.message);
    });
  }

  const getSimilarColleges = (cityName: string, stateName: string, course: string, studentCount: number) => {
    const baseurl = process.env.REACT_APP_API_BASEURL;
    const fullUrl = `${baseurl}/api/v1/college/similar/${cityName}/${stateName}/${course}/${studentCount}`;
    
    axios.get(fullUrl).then((res) => {
      setSimilarColleges(res.data.data);
    }).catch(err => {
      setErrorMsg(err?.response.data.error.message);
    });
  }

  const getSelectedCollege = (event: React.MouseEvent<HTMLDivElement, MouseEvent>, id: string) => {
    getCollegeDetails(id);
  }
  
  //   
  useEffect(() => {
    setOpen(props.openModel);
    getCollegeDetails(props.id);
  }, [props])
  // Functions

  const handleClose = () => {
    setOpen(false);
    props.onModelClose();
  };



  return (
    <React.Fragment>
      <Dialog 
        open={open} 
        onClose={handleClose}
        fullWidth={true}
        maxWidth={'md'} 
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title"></DialogTitle>
        <Container maxWidth="md">
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <h3>{'College Details'}</h3>
              <Divider className={classes.divider}/>
              <div>
                <FormLabel>College Name</FormLabel>
                <Typography>{collegeData?.name}</Typography>
              </div>
              <Divider className={classes.divider}/>
              <div>
                <FormLabel>Courses Offers</FormLabel>
                <div>
                  {collegeData?.coursesOffered? collegeData?.coursesOffered.map(course => {
                    return(
                      <Chip className={classes.chip} label={course} key={course}/>
                    );
                  }) : null}
                </div>
              </div>
              <Divider className={classes.divider}/>
              <div>
                <FormLabel>No. of Students</FormLabel>
                <Typography>{collegeData?.studentCount}</Typography>
              </div>
              <Divider className={classes.divider}/>
              <div>
                <FormLabel>Founded Year</FormLabel>
                <Typography>{collegeData?.yearFounded}</Typography>
              </div>
              <Divider className={classes.divider}/>
              <div>
                <FormLabel>City Name</FormLabel>
                <Typography>{collegeData?.cityName}</Typography>
              </div>
              <Divider className={classes.divider}/>
              <div>
                <FormLabel>State Name</FormLabel>
                <Typography>{collegeData?.stateName}</Typography>
              </div>
              <Divider className={classes.divider}/>
              <div>
                <FormLabel>Country Name</FormLabel>
                <Typography>{collegeData?.countryName}</Typography>
              </div>
            </Grid>
            <Grid item xs={6}>
              <h3>{'Similar Colleges'}</h3>
              <Divider className={classes.divider}/>
              <List>
                {similarColleges.map((college => {
                  return (<ListItem button  
                    onClick={(event) => getSelectedCollege(event, college._id)}
                    id={college._id}
                    key={college._id}
                  >
                    <ListItemText primary={college.name} />
                  </ListItem>)
                }))}
              </List>
            </Grid>
          </Grid>
        </Container>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}

export default ShowCollege;
