import React, { useEffect, useState } from 'react';
import { Grid } from '@material-ui/core';
import PieChart from './PieChart';
import axios from 'axios';

interface Data {
  stateWise: any[][];
  courseWise: any[][];
}

const Dashboard = () => {
  const [getDashboardData, setDashboarData] = useState<Data>({
    stateWise: [],
    courseWise: []
  });

  const getDashboard = () => {
    const baseurl = process.env.REACT_APP_API_BASEURL;
    // 
    const url: string = '/api/v1/college/dashboard';
    console.log('url=>', url);
    const fullUrl: string = baseurl+url;
      axios.get(fullUrl).then(res => {
        // console.log('res', resData);
        const resData: Data = res.data.data;
        setDashboarData(resData);
      }).catch(err=>{
        console.error('err', err);
      });
  }
     
  // Add Header
  const stateData = [["States", "Colleges"], ...getDashboardData.stateWise];
  const courseData = [["States", "Colleges"], ...getDashboardData.courseWise];
  // options
  const options = {
    course: {
      title: "Courses in Colleges",
      pieHole: 0.4,
      is3D: false,
    },
    state: {
      title: "Colleges in States",
      pieHole: 0.4,
      is3D: false,
    }
  };
  
  useEffect(()=>{
    getDashboard();
  }, []);

  return(
    <React.Fragment>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <PieChart data={stateData} options={options.state} />
        </Grid>
        <Grid item xs={6}>
          <PieChart data={courseData} options={options.course} />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}


export default Dashboard;