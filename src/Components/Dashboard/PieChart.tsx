import React, {useState, useEffect} from 'react';
import { Paper } from '@material-ui/core';
import { Chart } from "react-google-charts";
import { PieChartsData } from '../../Interfaces';


interface Props {
  data: any;
  options: any;
}

const PieChart = (props: Props) => {
    return (
        <Paper>
          <Chart
            chartType="PieChart"
            width="100%"
            height="300px"
            data={props.data}
            options={props.options}
          />
        </Paper>
      );
}

export default PieChart;
