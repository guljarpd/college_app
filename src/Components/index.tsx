import React from "react";
import Styled from 'styled-components';
import MenuBar from './Menu';
import Dashboard from './Dashboard';
import College from './College';
import Student from './Student';

const MainDiv = Styled.div`
  display: 'flex';
`;
const ContentDiv = Styled.div`
  margin-left: 100px;
  margin-top: 100px;
  overflow-y: auto !important;
  padding: 10px !important;
`;

export const DashboardRoute = () => {
    return(
        <MainDiv>
            <MenuBar />
            <ContentDiv>
                <Dashboard />
            </ContentDiv>
        </MainDiv>
    )
}
export const CollegeRoute = () => {
    return(
        <MainDiv>
            <MenuBar />
            <ContentDiv>
                <College />
            </ContentDiv>
        </MainDiv>
    )
}

export const StudentRoute = () => {
    return(
        <MainDiv>
            <MenuBar />
            <ContentDiv>
                <Student />
            </ContentDiv>
        </MainDiv>
    )
}