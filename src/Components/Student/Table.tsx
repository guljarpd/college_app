import React, {useEffect, useState} from 'react';
import { 
    Paper,
    Toolbar, 
    Typography, 
    Tooltip,
    IconButton,
    TablePagination,
    TableSortLabel,
    TableContainer,
    Table,
    TableBody,
    TableRow,
    TableCell,
    TableHead,
    TextField
} from '@material-ui/core';
import {
    AddCircleSharp
} from '@material-ui/icons';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Autocomplete from '@material-ui/lab/Autocomplete';
import AddStudent from './AddStudent';
import ShowStudent from './ShowDetails';
import { CollegeI, StudentI } from '../../Interfaces';
import axios from 'axios';
// Interfaces
interface TableDataStudent {
  _id: string;
  collegeId: string;
  name: string;
  batchYear: string;
  course: string;
  gender: string;
}
interface TableDataProps {
    collegeData: CollegeI[];
    selectedCollegeId: string;
}

type Order = 'asc' | 'desc';

interface TableToolbarProps {
    tableTitle: string;
    colleges: CollegeI[];
    selectedCollegeId: string;
    onSelectCollege: (selectedCollegeId: string) => void;
}

interface TableHeadProps {
    classes: ReturnType<typeof useToolbarStyles>;
    onRequestSort: (event: React.MouseEvent<unknown>, property: keyof TableDataStudent) => void;
    order: Order;
    orderBy: string;
}

interface HeadCell {
    id: keyof TableDataStudent,
    label: string;
}

// Custom css
const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
        width: '100%',
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 750,
    },
    title: {
        flex: '1 1 100%',
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
    header2: {
        fontWeight: 600
    },
    autoComplete: {
      display: 'contents'
    }
  }),
);

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
}

function getComparator<Key extends keyof any>(
    order: Order,
    orderBy: Key,
  ): (a: { [key in Key]: number | string }, b: { [key in Key]: number | string }) => number {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
}
  
function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
    const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}



const TableToolbar = (props: TableToolbarProps) => {
    const classes = useToolbarStyles();
    const [getModel, setModel] = useState<boolean>(false);
    const [selectedValue, setSelectedValue] = useState<CollegeI | null>(null);
    // Functions
    const defaultProps = {
      options: props.colleges,
      getOptionLabel: (option: CollegeI) => option.name,
    };
    // 
    const handleClick = () => {
        setModel(true);
    }
    const closeModel = () => {
        setModel(false);
    }

    const onSelectCollege = (college: CollegeI | null) => {
      if(college) {
        setSelectedValue(college);
        props.onSelectCollege(college._id);
      }
    }

    useEffect(() => {
      let getPreSelected = props.colleges.filter(item => {
        if(item._id === props.selectedCollegeId) return item;
      });
      if(getPreSelected.length>0) {
        setSelectedValue(getPreSelected[0]);
      }
    }, [props.colleges]);

    return (
      <React.Fragment>
        {getModel && selectedValue? <AddStudent openModel={true} onModelClose={closeModel} college={selectedValue} />: null}
        <Toolbar>
            <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
                {props.tableTitle}
            </Typography>
            <Autocomplete
              {...defaultProps}
              value={selectedValue}
              onChange={(event: any, item: CollegeI | null) => {
                onSelectCollege(item);
              }}
              id="auto-complete"
              className={classes.autoComplete}
              autoComplete
              includeInputInList
              renderInput={(params) => <TextField {...params} label="Select College" margin="normal" />}
            />
            <Tooltip title="Add Student">
                <IconButton 
                    color={'secondary'} 
                    aria-label="Add Student"
                    onClick={handleClick}
                >
                    <AddCircleSharp />
                </IconButton>
            </Tooltip>
        </Toolbar>
      </React.Fragment>
    );
};

const headCells: HeadCell[] = [
    {id: 'name', label: 'Name'},
    {id: 'course', label: 'Course'},
    {id: 'gender', label: 'Gender'},
    {id: 'batchYear', label: 'Batch Year'},
]
  
const CustomTableHead = (props: TableHeadProps) => {
    const { classes, order, orderBy, onRequestSort } = props;
    const createSortHandler = (property: keyof TableDataStudent) => (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };
    return (
        <TableHead>
          <TableRow>
            {headCells.map((item) => (
              <TableCell
                key={item.id}
                sortDirection={orderBy === item.id ? order : false}
              >
                <TableSortLabel
                  className={classes.header2}
                  active={orderBy === item.id}
                  direction={orderBy === item.id ? order : 'asc'}
                  onClick={createSortHandler(item.id)}
                >
                  {item.label}
                  {orderBy === item.id ? (
                    <span className={classes.visuallyHidden}>
                      {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                    </span>
                  ) : null}
                </TableSortLabel>
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
    );
}


const StudentTable = (props: TableDataProps) => {
    // Stores
    const [studentData, setStudentData] = useState<TableDataStudent[]>([]);
    const [getStudents, setStudents] = useState<StudentI[]>([]);
    const [rowsPerPage, setRowsPerPage] = useState<number>(10);
    const [page, setPage] = useState<number>(0);
    const [order, setOrder] = useState<Order>('asc');
    const [orderBy, setOrderBy] = useState<keyof TableDataStudent>('name');
    const [selectedStudentId, setSelectedStudentId] = useState<string | null>(null);

    // Styles
    const classes = useToolbarStyles();
    // Functions
    const getStudentData = (id: string) => {
      if(!id) return; // retrun if college id not found
      // 
      const baseurl = process.env.REACT_APP_API_BASEURL;
      const fullUrl: string = `${baseurl}/api/v1/college/students/${id}`;
      axios.get(fullUrl).then(res => {
        // console.log('res', res.data.data);
        if(res.data.data) {
          setStudents(res.data.data);
          setTableData(res.data.data);
        }
      }).catch(err=>{
        console.error('err', err);
      });
    }

    const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof TableDataStudent) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
    };
    // 
    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleClick = (event: React.MouseEvent<unknown>, id: any) => {
        // when click on this/ Open an Modal pop.
        console.log('handleClick=>', id);
        setSelectedStudentId(id);
    }
    const closeModel = () => {
        setSelectedStudentId('');
    }

    const setTableData = (students: StudentI[]) => {
      let tableData = students.map(item => {
          delete item?.skills;
          return item;
      });
      setStudentData(tableData);
    }

    const handleOnSelectCollege = (collegeId: string) => {
      getStudentData(collegeId);
    }
    // 
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, studentData.length - page * rowsPerPage);
    // 
    let sortedStudentData = stableSort(
        studentData, 
        getComparator(order, orderBy)
    ).slice(
        (page * rowsPerPage), 
        (page * rowsPerPage + rowsPerPage)
    );
    // 
    useEffect(() => {
      getStudentData(props.selectedCollegeId)
    }, [props.collegeData]);

    return (
        <div className={classes.root}>
            {selectedStudentId? (
                <ShowStudent 
                    id={selectedStudentId} 
                    openModel={true} 
                    onModelClose={closeModel} 
                />)
                : null
            }
            <Paper className={classes.paper}>
                <TableToolbar 
                  tableTitle={'Students'} 
                  colleges={props.collegeData}
                  selectedCollegeId = {props.selectedCollegeId}
                  onSelectCollege = {handleOnSelectCollege} 
                />
                <TableContainer> 
                   <Table
                    className={classes.table}
                    aria-labelledby="tableTitle"
                    size={'medium'}
                    aria-label="enhanced table"
                   >   <CustomTableHead 
                            classes={classes}
                            order={order}
                            orderBy={orderBy}
                            onRequestSort={handleRequestSort}
                        />
                       <TableBody>
                            {sortedStudentData.map((row, _index) => {
                                return (
                                    <TableRow
                                        hover={true}
                                        onClick={(event) => handleClick(event, row._id)}
                                        role="button"
                                        key={row._id}
                                    >
                                        <TableCell align="left">{row.name}</TableCell>
                                        <TableCell align="left">{row.course}</TableCell>
                                        <TableCell align="left">{row.gender}</TableCell>
                                        <TableCell align="left">{row.batchYear}</TableCell>
                                    </TableRow>
                                );
                            })}
                            {emptyRows > 0 && (
                                <TableRow style={{ height: (53 * emptyRows) }}>
                                  <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>
                    </Table> 
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 15]}
                    component="div"
                    count={studentData.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </Paper>
        </div>
    )
}

export default StudentTable;