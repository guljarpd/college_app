import React, {useState, useEffect} from 'react';
import {
    Button,
    Dialog,
    DialogActions,
    DialogTitle,
    Chip,
    Grid,
    Container,
    Typography,
    FormLabel,
    Divider
} from '@material-ui/core';

import { createStyles, makeStyles, useTheme, Theme } from '@material-ui/core/styles';
import axios from 'axios';
import { StudentI, CollegeI } from '../../Interfaces';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    chips: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    chip: {
      margin: 2,
    },
    noLabel: {
      marginTop: theme.spacing(3),
    },
    divider: {
      marginTop: 10,
      marginBottom: 10
    }
  }),
);

// interfaces
interface ShowStudentProps {
    openModel: boolean;
    id: string;
    onModelClose: () => void;
}

const  getStyles = (name: string, personName: string[], theme: Theme) => {
    return {
      fontWeight:
        personName.indexOf(name) === -1
          ? theme.typography.fontWeightRegular
          : theme.typography.fontWeightMedium,
    };
}


const ShowStudent = (props: ShowStudentProps) => {
  const [open, setOpen] = useState(true);
  const [studentData, setStudentData] = useState<StudentI | null>(null);
  const [collegeName, setCollegeName] = useState<string>('');
  const [errorMsg, setErrorMsg] = useState<string>('');
  //   
  const classes = useStyles();
  const theme = useTheme();

  const getStudentDetails = (id: string) => {
    const baseurl = process.env.REACT_APP_API_BASEURL;
    const fullUrl = baseurl+'/api/v1/student/details/'+id;
    axios.get(fullUrl).then((res) => {
        let data: StudentI = res.data.data;
        setStudentData(data);
        getCollegeName(data.collegeId);
      }).catch(err => {
        setErrorMsg(err?.response.data.error.message);
    });
  }

  const getCollegeDetails = async (id: string) => {
    try {
      const baseurl = process.env.REACT_APP_API_BASEURL;
      const fullUrl = baseurl+'/api/v1/college/details/'+id;
      const college = await axios.get(fullUrl);
      return (college.data.data? college.data.data.name : null);
    } catch (error: any) {
      return null;
    }
  }


  // 
  const getCollegeName = async (collegeId: string) => {
    let localStorageCollegeData = await localStorage.getItem('CollegeList');
    if(localStorageCollegeData){
      let data: CollegeI[] = JSON.parse(localStorageCollegeData);
      var college = data.filter(item => {
        if (item._id === collegeId) return item;
      });
      if(college.length> 0) {
        setCollegeName(college[0].name)
      }else {
        setCollegeName('');
      }
      return;
    } else {
      // get from server
      let name = await getCollegeDetails(collegeId);
      if(name) {
        setCollegeName(name);
      }else {
        setCollegeName('');
      }
    }
  }
  //   
  useEffect(() => {
    setOpen(props.openModel);
    getStudentDetails(props.id);
  }, [props])
  // Functions

  const handleClose = () => {
    setOpen(false);
    props.onModelClose();
  };



  return (
    <React.Fragment>
      <Dialog 
        open={open} 
        onClose={handleClose}
        fullWidth={true}
        maxWidth={'md'} 
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title"></DialogTitle>
        <Container maxWidth="md">
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <h3>{'Student Details'}</h3>
              <Divider className={classes.divider}/>
              <div>
                <FormLabel>Student Name</FormLabel>
                <Typography>{studentData?.name}</Typography>
              </div>
              <Divider className={classes.divider}/>
              <div>
                <FormLabel>Gender</FormLabel>
                <Typography>{studentData?.gender}</Typography>
              </div>
              <Divider className={classes.divider}/>
              <div>
                <FormLabel>College Name</FormLabel>
                <Typography>{collegeName}</Typography>
              </div>
              <Divider className={classes.divider}/>
              <div>
                <FormLabel>Course</FormLabel>
                <Typography>{studentData?.course}</Typography>
              </div>
              <Divider className={classes.divider}/>
              <div>
                <FormLabel>Batch Year</FormLabel>
                <Typography>{studentData?.batchYear}</Typography>
              </div>
              <Divider className={classes.divider}/>
              <div>
                <FormLabel>Skills</FormLabel>
                <div>
                  {studentData?.skills? studentData?.skills.map(course => {
                    return(
                      <Chip className={classes.chip} label={course} key={course}/>
                    );
                  }) : null}
                </div>
              </div>
            </Grid>
          </Grid>
        </Container>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}

export default ShowStudent;
