import React, {useEffect, useState} from 'react';
import { useQuery } from '../../Utils/hooks';
import { CollegeI, StudentI } from '../../Interfaces';
import axios from 'axios';
import StudentTable from './Table';


export default function Student() {

  const queryParams = useQuery();
  const collegeId = queryParams.get('clg_id');
  const [selectedCollegeId, setSelectedCollegeId] = useState<string | null>(null);
  const [getColleges, setColleges] = useState<CollegeI[]>([]);
  const [getError, setError] = useState<string>('');
  // 

  // 
  const getAllCollegeData = () => {
    // check from local state.
    let localStorageCollegeData = localStorage.getItem('CollegeList');
    if(localStorageCollegeData){
      let data: CollegeI[] = JSON.parse(localStorageCollegeData)
      setColleges(data);
      if(!collegeId) setSelectedCollegeId(data[0]._id);
      return true;
    }
    // 
    const baseurl = process.env.REACT_APP_API_BASEURL;
    const url: string = '/api/v1/college/all';
    if(baseurl){
      const fullUrl: string = baseurl+url;
      axios.get(fullUrl).then(res => {
        // console.log('res', resData);
        const resData: CollegeI[] = res.data.data;
        setColleges(resData);
        if(!collegeId) setSelectedCollegeId(resData[0]._id);
        // 
        localStorage.setItem('CollegeList', JSON.stringify(resData));
        return true;
      }).catch(err=>{
        console.error('err', err);
        return false;
      });
    }
  }
  // 
  useEffect(()=> {
    getAllCollegeData();
    setSelectedCollegeId(collegeId);
  }, [collegeId]);

  // default
  useEffect(()=> {
    getAllCollegeData();
  }, []);
   
  return (
    <React.Fragment>
      {selectedCollegeId?
        <StudentTable 
          selectedCollegeId = {selectedCollegeId} 
          collegeData={getColleges}
        />
      : null}
    </React.Fragment>
  );
}
