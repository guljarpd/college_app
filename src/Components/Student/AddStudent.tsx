import React, {useState, useEffect} from 'react';
import {
    Button,
    TextField,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Select,
    MenuItem,
    FormControl,
    InputLabel,
    Input,
    Chip,
    Snackbar
} from '@material-ui/core';

import { createStyles, makeStyles, useTheme, Theme } from '@material-ui/core/styles';
import axios from 'axios';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import { CollegeI } from '../../Interfaces';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    chips: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    chip: {
      margin: 2,
    },
    noLabel: {
      marginTop: theme.spacing(3),
    },
  }),
);

// interfaces
interface AddStudentProps {
    openModel: boolean;
    college: CollegeI;
    onModelClose: () => void;
}

interface StudentForm {
    collegeId?: string;
    name?: string;
    batchYear?: string;
    skills?: string[];
    course?: string;
    gender?: string;
}

// skills
const skillsList: string[] = [
	"C",
	"C++",
	"Java",
	"Nodejs",
	"Python",
	"Elixir",
	"HTML",
	"CSS",
	"PostgreSQL",
	"MySQL",
	"MongoDB",
	"Redis",
	"GCP",
	"AWS",
	"Digital Ocean",
	"DevOps",
	"Docker"
];

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const  getStyles = (name: string, personName: string[], theme: Theme) => {
    return {
      fontWeight:
        personName.indexOf(name) === -1
          ? theme.typography.fontWeightRegular
          : theme.typography.fontWeightMedium,
    };
}

export const Alert = (props: AlertProps) => {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}




const AddStudent = (props: AddStudentProps) => {
  const [open, setOpen] = useState(true);
  const [formData, setFormData] = useState<StudentForm>();
  const [getSkills, setSkills] = useState<string[]>([]);
  const [successMsg, setSuccessMsg] = useState<string>('');
  const [errorMsg, setErrorMsg] = useState<string>('');
  const [alertOpen, setAlertOpen] = useState<boolean>(false);
  //   
  const classes = useStyles();
  const theme = useTheme();
  
  //   
  useEffect(() => {
    setOpen(props.openModel);
    setFormData({
      ...formData,
      collegeId: props.college._id
    })
  }, [props])

  // Functions
  const handleSave = () => {
    const baseurl = process.env.REACT_APP_API_BASEURL;
    const fullUrl = baseurl+'/api/v1/student/add';
    // college to db.
    // console.log('handleSave', formData);
    axios.post(fullUrl, formData)
      .then((res) => {
        if(res.data.data) {
          // show success message.
          setSuccessMsg('Success! Student Added.')
          setAlertOpen(true);
        }
      })
      .catch((err) => {
        setErrorMsg(err?.response.data.error.message)
        setAlertOpen(true);
      })
  };

  const handleClose = () => {
    setOpen(false);
    props.onModelClose();
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    console.log(event.target?.name);
    setFormData(
        {
            ...formData,
            [event.target?.name]: event.target?.value
        }
    )
  };

  const handleSelectChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    
    let name = (event.target as HTMLInputElement).name;
    setFormData({
      ...formData,
      [name]: event.target.value as string
    });
  }

  const handleChipChange = async (event: React.ChangeEvent<{ value: unknown }>) => {
    setSkills(event.target.value as string[]);
    // 
    setFormData({
        ...formData,
        skills: event.target.value as string[]
    });
  };

  const handleCloseAlert = () => {
    setAlertOpen(false);
    setSuccessMsg('');
    setErrorMsg('');
  }

  return (
    <React.Fragment>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Add Student</DialogTitle>
        {errorMsg !== ''? (
          <Snackbar open={alertOpen} autoHideDuration={6000} onClose={handleCloseAlert}>
            <Alert onClose={handleCloseAlert} severity="error">
              {errorMsg}
            </Alert>
          </Snackbar>
        ) : null}
        {successMsg !== ''? (
          <Snackbar open={alertOpen} autoHideDuration={6000} onClose={handleCloseAlert}>
            <Alert onClose={handleCloseAlert} severity="success">
              {successMsg}
            </Alert>
          </Snackbar>
        ) : null}
        <DialogContent>
          <DialogContentText>
            Add a new college to database.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Student Name"
            type="text"
            name="name"
            value={formData?.name}
            fullWidth
            onChange={handleInputChange}
          />
          <TextField
            margin="dense"
            id="batchYear"
            name="batchYear"
            label="Batch Year"
            type="number"
            value={formData?.batchYear}
            fullWidth
            onChange={handleInputChange}
          />
          <FormControl 
            margin="dense"
            fullWidth
          >
            <InputLabel id="demo-simple-select-label">Gender</InputLabel>
            <Select
                labelId="demo-simple-select-helper-label"
                id="demo-simple-select-helper"
                value={formData?.gender}
                name="gender"
                onChange={handleSelectChange}
            >
                <MenuItem value={'Male'}>{'Male'}</MenuItem>
                <MenuItem value={'Female'}>{'Female'}</MenuItem>
                <MenuItem value={'Other'}>{'Other'}</MenuItem>
            </Select>
          </FormControl>
          <FormControl 
            margin="dense"
            fullWidth
          >
            <InputLabel id="demo-simple-select-label">Course</InputLabel>
            <Select
                labelId="demo-simple-select-helper-label"
                id="demo-simple-select-helper"
                value={formData?.course}
                name="course"
                onChange={handleSelectChange}
            > 
              {props.college?.coursesOffered && props.college?.coursesOffered.map(course => {
                return(<MenuItem key={course} value={course}>{course}</MenuItem>)
              })}
            </Select>
          </FormControl>
          <FormControl margin="dense" fullWidth>
                <InputLabel id="demo-mutiple-chip-label">Skills</InputLabel>
                <Select
                labelId="demo-mutiple-chip-select-label"
                id="demo-mutiple-chip"
                multiple
                value={getSkills}
                onChange={handleChipChange}
                input={<Input id="select-multiple-chip" />}
                renderValue={(selected) => (
                    <div className={classes.chips}>
                    {(selected as string[]).map((value) => (
                        <Chip key={value} label={value} className={classes.chip} />
                    ))}
                    </div>
                )}
                MenuProps={MenuProps}
                >
                {skillsList.map((name) => (
                    <MenuItem key={name} value={name} style={getStyles(name, getSkills, theme)}>
                    {name}
                    </MenuItem>
                ))}
                </Select>
            </FormControl>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
          <Button onClick={handleSave} color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}

export default AddStudent;
