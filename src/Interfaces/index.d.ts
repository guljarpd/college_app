export interface PieChartsData {
    label: string;
    value: number
}


// College
export interface CollegeI {
    _id: string;
    name: string;
    yearFounded: string;
    coursesOffered?: string[];
    cityName: string;
    stateName: string;
    countryName: string;
    studentCount: number;
    __v: number;
    createdAt: Date;
    updatedAt: Date;
}

export interface CollegeObjectI {
    data: College[];
}

export interface StudentI {
    _id: string;
    collegeId: string;
    name: string;
    batchYear: string;
    skills?: string[];
    course: string;
    gender: string;
    createdAt: Date;
    updatedAt: Date;
    __v: number;
}

export interface StudentObjectI {
    data: StudentI[];
}
